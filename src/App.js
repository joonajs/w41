import React from 'react';
import Interval from './Interval';
import './App.css';

function App() {
  return (
    <div className="App">
      <Interval />
    </div>
  );
}

export default App;
