import React, { useState } from 'react';
import './App.css';

const Interval = () => {
    const [intervalTimes, setIntervalTimes] = useState([]);

    const recordIntervalTime = () => {
        const currentTime = Date.now();
        setIntervalTimes(prevTimes => [...prevTimes, currentTime]);
    };

    const formatTime = (time) => {
        const date = new Date(time);
        const hours = date.getHours().toString().padStart(2, '0');
        const minutes = date.getMinutes().toString().padStart(2, '0');
        const seconds = date.getSeconds().toString().padStart(2, '0');
        let milliseconds = date.getMilliseconds().toString();
        if (milliseconds.length === 1) {
            milliseconds = `00${milliseconds}`;
        } else if (milliseconds.length === 2) {
            milliseconds = `0${milliseconds}`;
        }
        return `${hours}:${minutes}:${seconds}.${milliseconds}`;


        
    };

    return (
        <div className='App'>
            <h1>w41</h1>
            <button style={{ fontSize: 36 }} onClick={recordIntervalTime}>
                Interval time
            </button>
            <table>
                <tbody>
                    {intervalTimes.map((time, index) => (
                        <tr key={index}>
                            <td>{formatTime(time)}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default Interval;
